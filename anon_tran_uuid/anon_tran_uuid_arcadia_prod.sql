\connect arcadia_ops

CREATE OR REPLACE FUNCTION "email_removal"."anon_tran_uuid"("_uuid0" uuid, "_email0" text, "_debug" bool=false)
  RETURNS "pg_catalog"."varchar" AS $BODY$
/* v1  - 26/06/18 */
/* v1.1- 02/07/18 - moved to email_removal schema */
/* v1.2- 02/07/18 - detects table and updates accordingly */
/* v1.3- 03/07/18 - returns number of tables updates or -1 for error */
/* v1.4           - tested in arcadia, UniqloUS test envs */
/* v1.5  04/07/18 - 1st prod version (changed temp tables to actual tables) */
/* v1.6  10/07/18 - removed RAISE NOTICE debug statements */
/* v1.7  11/07/18 - added optional DEBUG parameter */
/* v1.71 18/07/18 - return -2 if no transaction found */
/* v1.72 31/07/18 - ignore case of email */
declare
  pos1_otd int    := 0;
  pos2_otd int    := 0;
  pos1_plg int    := 0;
  pos2_plg int    := 0;
  pos1_xml int    := 0;
  pos2_xml int    := 0;
  _poslog xml     := NULL;
  _poslog2 xml    := NULL;
	_poscolumn text := '';
  _reqxml xml     := NULL;
  _otdtext text   := '';
  _email_otd text := '';
  _email_xml text := '';
  _email_plg text := '';
	_table_status bool[] := ARRAY[true, true, true];  /* 1 = mcore5_transactions, 2=mcore5_app_req, 3=z_mcore_pos_trans */
	_updateCount int := 0;
	_retailerId int := 0;
	_EmailXMLStart text := NULL;
	_EmailXMLEnd   text := NULL;
BEGIN
_email0 = lower(_email0);
if _debug then RAISE NOTICE 'Start...'; end if;

  /* gather data from mcore5_transactions (if it exists) */
  begin
    select poslog, request_xml into _poslog, _reqxml
	  from public.mcore5_transactions where transaction_uuid = _uuid0;
	exception when others then
    _table_status[1] = false;
	end;

  /* gather data from mcore5_app_requests (if it exists) */
  begin
    select convert_from(data,'utf8') into _otdtext
		from public.mcore5_app_requests where transaction_uuid = _uuid0;
	exception when others then
    _table_status[2] = false;
	end;

  /* gather data from z_mcore_pos_transactions (if it exists) */
  begin
	  select column_name into _poscolumn from information_schema.columns 
    where table_name='z_mcore_pos_transactions' and column_name='poslog';

    if (_poscolumn = 'poslog') then
      select convert_from(original_transaction_data,'utf8'), poslog, retailer_id into _otdtext, _poslog2, _retailerId
		  from public.z_mcore_pos_transactions where transaction_uuid = _uuid0;
		else
      select convert_from(original_transaction_data,'utf8'), retailer_id into _otdtext, _retailerId
		  from public.z_mcore_pos_transactions where transaction_uuid = _uuid0;
		end if;
	exception when others then
    _table_status[3] = false;
	end;
  _poslog = coalesce(_poslog, _poslog2);

  if (_retailerId='99') then    /* Arcadia */
	  _EmailXMLStart = 'Email>';
	  _EmailXMLEnd   = '/Email>';
	else
	  _EmailXMLStart = 'email>';
	  _EmailXMLEnd   = '/email>';
	end if;
	
if _debug then
RAISE NOTICE 'RetailerID=%.', _retailerId;
RAISE NOTICE 'mcore5_tran found=% | mcore5_req found=% | z_mcore_pos_tran found=%', _table_status[1], _table_status[2], _table_status[3];
RAISE NOTICE 'len _otdtext=% len _poslog=%. len _reqxml=%.', length(_otdtext), length(_poslog::text), length(_reqxml::text);
end if;

  if coalesce(_otdtext, _poslog::text, _reqxml::text) is NULL THEN
if _debug then RAISE NOTICE 'Transaction not Found!'; end if;
    RETURN -2;
  end if;

  if (length(_otdtext)>0) then
    pos1_otd   = position(_EmailXMLStart in _otdtext)+6;
    pos2_otd   = position(_EmailXMLEnd   in _otdtext)-1;
    _email_otd = lower(trim(substring(_otdtext from pos1_otd for pos2_otd-pos1_otd)));
	else
	  _email_otd = '';
	end if;
if _debug then RAISE NOTICE 'otd start=% end=% len=% lower Trim email >%<', pos1_otd, pos2_otd, pos2_otd-pos1_otd, _email_otd; end if;
	
  if (length(_reqxml::text)>0) then
    pos1_xml   = position(_EmailXMLStart in _reqxml::text)+6;
    pos2_xml   = position(_EmailXMLEnd   in _reqxml::text)-1;
    _email_xml = lower(trim(substring(_reqxml::text from pos1_xml for pos2_xml-pos1_xml)));
	else
	  _email_xml = '';
	end if;
if _debug then RAISE NOTICE 'xml start=% end=% len=% lower Trim email >%<', pos1_xml, pos2_xml, pos2_xml-pos1_xml, _email_xml; end if;

  if (length(_poslog::text)>0) then
    pos1_plg   = position(':EMail>'  in _poslog::text)+7;
    pos2_plg   = position(':EMail><' in _poslog::text)-5;
    _email_plg = lower(trim(substring(_poslog::text from pos1_plg for pos2_plg-pos1_plg)));
	else
	  _email_plg = '';
	end if;
if _debug then RAISE NOTICE 'plg start=% end=% len=% lower Trim email >%<', pos1_plg, pos2_plg, pos2_plg-pos1_plg, _email_plg; end if;

if _debug then RAISE NOTICE '_email0 >%< _email_xml >%< _email_plg >%< _email_otc >%<.', _email0, _email_xml, _email_plg, _email_otd; end if;
  if ( (_email0!=_email_otd AND _email_otd!='') OR
	     (_email0!=_email_xml AND _email_xml!='') OR
			 (_email0!=_email_plg AND _email_plg!='')  ) THEN
if _debug then RAISE NOTICE 'email parameter NO MATCH.  Exiting...'; end if;
    RETURN -1;
  end if;

  if ( _email_xml!='') then
    _reqxml = overlay (_reqxml::text placing ' ' from pos1_xml for (pos2_xml-pos1_xml))::xml;
  end if;
  if ( _email_plg!='') then
    _poslog = overlay (_poslog::text placing ' ' from pos1_plg for (pos2_plg-pos1_plg))::xml;
  end if;
	if ( _email_otd!='') then
	  _otdtext = overlay(_otdtext      placing ' ' from pos1_otd for (pos2_otd-pos1_otd))::xml;
  end if;

  if (_table_status[1] and (_email_plg!='' or _email_xml!='') ) then   /* mcore5_transactions */
if _debug then RAISE NOTICE 'About to update mcore5_transactions.'; end if;
	  _updateCount = _updateCount+1;
    update public.mcore5_transactions
      set request_xml = _reqxml, poslog = _poslog
    where transaction_uuid = _uuid0;
  end if;
		
  if (_table_status[2] and _email_xml!='' ) then                /* mcore5_app_requests */
if _debug then RAISE NOTICE 'About to update mcore5_app_requests.'; end if;
	  _updateCount=_updateCount+1;
    update public.mcore5_app_requests
	  set data = convert_to(_reqxml::text,'utf-8')
    where transaction_uuid = _uuid0;
	end if;

  if (_table_status[3] and _email_otd!='' ) then               /* z_mcore_pos_transactions */
if _debug then RAISE NOTICE 'About to update z_mcore_pos_transactions.';  end if;
	  _updateCount=_updateCount+1;
    if (_poscolumn = 'poslog') then
      update public.z_mcore_pos_transactions
	     set original_transaction_data = convert_to(_otdtext,'utf-8'), poslog = _poslog
      where transaction_uuid = _uuid0;
	  else
      update public.z_mcore_pos_transactions
	     set original_transaction_data = convert_to(_otdtext,'utf-8')
      where transaction_uuid = _uuid0;
	  end if;
			
	end if;
if _debug then RAISE NOTICE 'Finish.'; end if;
  RETURN _updateCount;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  security definer;
  
