#!/usr/bin/perl 

use strict;
use warnings;

use DBI;
use Getopt::Long;
use File::Basename;
use IO::Compress::Gzip qw(gzip $GzipError);

# Database row count
# Counts the number of rows which would be selected based on an input date (YYYY-MM-DD)
# 22/08/18 - this functionality is not built into ex1b.pl (ops_db_trimmer_pseudo.pl) using the -c option

my ($help,$dbname,$retailer,$datecutoff);

Getopt::Long::Configure ("bundling", "ignorecase");

GetOptions(
    
    "help|?" => \$help,
    "database|d=s" => \$dbname,
    "retailer|r=s" => \$retailer,
    "datecutoff|D=s" => \$datecutoff,
) or die "Could not parse command line arguments\n";
   
sub usage_info {
   die  "You must provide the required parameters:\n$0 [parameters]
   -r|--retailer <retailer_name>
   -d|--database <database>
   -D|--datecutoff <YYYY-MM-DD>

   -?|--help
   \n";
}

# Default tables to process
my @tables = qw(z_mcore_digital_receipt_status z_mcore_digital_receipt_reports z_mcore_pos_transaction_commits z_mcore_pos_transactions z_mcore_trace_logs);

usage_info() unless  ($dbname && @tables && $retailer && $datecutoff);

my $t=localtime(time);
print "\n\n$t:Start: Database Row Counter\n\n";

print "dbname =>$dbname<.\n";
print "retailer =>$retailer<.\n";
print "dateorigin =>$datecutoff<.\n";

$t=localtime(time);

print "$t: INFO Database $dbname\n";
print "$t: INFO: Retailer = $retailer\n";
print "$t: INFO: Date Cutoff = $datecutoff\n\n";

# gets details from ~/.pg_service.conf
my $dbh = DBI ->connect("dbi:Pg:service=$dbname",  "", "", {AutoCommit => 1, RaiseError => 1})
  or die $DBI::errstr;

foreach my $tbl (@tables){
   my ($filter,$this_file_name,@ids2backup,$allmatchids);

   print "  Projected count on $tbl is: ";

      my $mpt_flag = 0;  #false
      my $sql = qq{SELECT count(*) FROM $tbl WHERE created_at < '$datecutoff'};
      if ("z_mcore_pos_transactions" eq $tbl) {
        $sql = $sql . " AND retailer_id=$retailer";
        $mpt_flag = 1;  #true
      }
#print "DBprepare >$sql<\n";
      my $sth = $dbh->prepare($sql) or die "Couldn't prepare statement: $DBI::errstr; stopped";
      $sth->execute() or die "Couldn't execute statement: $DBI::errstr; stopped";

      # Fetch each row and print it
      while ( my ($count1) = $sth->fetchrow_array() ) {
        print "$count1 ($mpt_flag)\n";
        if ($mpt_flag and 0==$count1) { printf "** No Rows.  Correct retailer? **\n"; }
      }     
}
$dbh->disconnect;

$t=localtime(time);
print "\n$t: The End.\n\n";

