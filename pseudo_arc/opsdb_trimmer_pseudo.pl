#!/usr/bin/perl 

use strict;
use warnings;
use DBI;
use Getopt::Long;
use File::Basename;
use IO::Compress::Gzip qw(gzip $GzipError);

# functions
# from: https://stackoverflow.com/questions/15109917/passing-variables-to-a-perl-subroutine

# ----------------------------------------------------------
# initialisation

my (@ptables,$plocaldir,$pbucket,$delete,$truncate,$backupdir,$help,$dbname,$retailer);

our ($datecutoff, $countonly);

# Local directory to storage copied record (file)
our $localdir = "./";
$localdir = $plocaldir if $plocaldir;

Getopt::Long::Configure ("bundling", "ignorecase");

GetOptions(
    "help|?" => \$help,
    "database|d=s" => \$dbname,
    "retailer|r=s" => \$retailer,
    "datecutoff|D=s" => \$datecutoff,
    "backup|b:s" => \$backupdir,
    "delete" => \$delete,
    "truncate" => \$truncate,
    "s3-bucket|s:s" => \$pbucket,
    "localdir|l:s" => \$plocaldir,
    "countonly|c" => \$countonly,
    "tables|t:s" => \@ptables,
) or die "Could not parse command line arguments\n";

usage_info() unless ($dbname && $datecutoff);

our $dbname_service    = $dbname . "_service";
our $dbname_service_pp = $dbname . "_pp_service";
our ($pp_host, $pp_port, $pp_user);
our $retailerId = 67;                            # temporary
our @file2upload;
our %table_count = ();

my @tablesPP  = qw(z_mcore_digital_receipt_reports z_mcore_digital_receipt_status z_mcore_pos_digital_receipt_requests z_mcore_pos_transactions );
my @tablesScanner  = qw(z_mcore_digital_receipts_reports_scanner z_mcore_digital_receipt_status_scanner z_mcore_pos_digital_receipt_requests_scanner z_mcore_pos_transactions_scanner);
# note: will need to archive mcore5_transactions also on newer MCOREs

our @tablesAll = qw(z_mcore_digital_receipt_reports z_mcore_digital_receipt_status z_mcore_pos_digital_receipt_requests z_mcore_pos_transactions z_mcore_trace_logs z_mcore_pos_transaction_commits);

our @nonPP_tables = qw(z_mcore_pos_transaction_commits z_mcore_trace_logs);

# ----------------------------------------------------------
# main
print "Start...\n\n";

retrieve_host_user_port($dbname_service_pp);

if ($countonly) {
  count_projected_rows($dbname_service,    @tablesAll);
  count_projected_rows($dbname_service_pp, @tablesPP);
  print "Exiting (count only) ...\n";
  exit(2);
}

# only need to set locks after countonly
my $lockfile_s;
engage_lock();

#-----------------    service,     filter?, table list
archive_table_data($dbname_service, 1, @nonPP_tables);

foreach my $tbl (@tablesPP) {
  print "\ntable = $tbl\n";
  transfer_table_data( $tbl);
}

call_pseudo_process();
archive_table_data($dbname_service_pp, 0, @tablesPP);
transfer_to_s3();

delete_table_data($dbname_service, @nonPP_tables);
delete_table_data($dbname_service, @tablesPP);

truncate_table_data($dbname_service_pp, @tablesPP);
truncate_table_data($dbname_service_pp, @tablesScanner);

print "The End.\n\n";
release_lock();

exit(0);

# ------------------------------------
sub release_lock {

  my $t = localtime(time);
  unlink $lockfile_s or warn "$t: Could not delete lock file $lockfile_s: $!\n";
}

# ------------------------------------
sub engage_lock {

  my $t1 = localtime(time);

  my ($name,$path,$suffix) = fileparse($0,".pl");
  $lockfile_s="/tmp/$name" . ".$dbname" . ".lock";
#print "\$name >$name<. ($path $suffix)\n\n";
#print "\$lockfile_s >$lockfile_s<.\n";

  # check process is not already running.
  if ( -e $lockfile_s){
     printf ("%s: WARN found another process with lock file $lockfile_s for $dbname. Aborting....\n", timenow());
     exit(-1);
  }
  else {
     # create our lock file
     open LOCKFL, ">$lockfile_s" or die "Could not create lock file:$!\n";
     close LOCKFL;
  }
}

# ------------------------------------
sub timenow {
  my $t1 = localtime(time);
  return $t1;
}

# ----------------------------------------------------------
sub transfer_to_s3 {

  print  "---------------------------------------------------------------\n";
  printf("(pid:$$) %s: start: transfer_to_s3()\n", timenow() );
  printf("%s: %d file(s) to upload to s3 @file2upload\n", timenow(), scalar(@file2upload) );

  my $returnCode = system("mv @file2upload tempdir");
  print "\nTransfer returned: $returnCode\n\n";

  printf("(pid:$$) %s:   end: transfer_to_s3()\n", timenow() );
  print "---------------------------------------------------------------\n";
}


# ----------------------------------------------------------
# $1 = table
sub call_pseudo_process {

  print  "---------------------------------------------------------------\n";
  printf("(pid:$$) %s: Start: Pseudonymisation process.\n", timenow());
  print  "---------------------------------------------------------------\n";

  my $retry = 0;
  my $sleeptime = 5;
  while ($retry < 5) {
    # call Pseudonymisation process jar file
    my $params = "-Ddb.hostname=$pp_host -Ddb.port=$pp_port -Ddb.name=$dbname -Ddb.username=$pp_user";
#print "\njava $params -jar jars/data-pseudonymiser-executable_generic.jar\n\n";
    my $returnCode = int( system("java $params -jar jars/data-pseudonymiser-executable_generic.jar")>>8);
    print "PP returned: $returnCode\n\n";
    if ($returnCode != 0) {
      print "*** WARNING PP process error *** retrying after $sleeptime seconds...\n";
      sleep($sleeptime);
      $sleeptime *= 2;
      $retry++;
    } else {
      last;
    }
  }
  if ($retry >=5) {
    print "Pseudo Process retry count exhausted.  Exiting ...\n";
    release_lock();
    exit(3);
  }

  print  "---------------------------------------------------------------\n";
  printf("(pid:$$) %s:   End: Pseudonymisation process.\n", timenow());
  print  "---------------------------------------------------------------\n";
}


# ----------------------------------------------------------
# see: https://stackoverflow.com/questions/36007390/why-to-shift-bits-8-when-using-perl-system-function-to-execute-command
# $1 = table
sub transfer_table_data {

  my ($table) = @_;

  my $psqlCommand = "PGSERVICE=$dbname_service psql -c \"\\copy (select * from $table WHERE created_at < '$datecutoff') to stdout with csv\" | PGSERVICE=$dbname_service_pp psql -c \"\\copy $table from stdin csv\"; exit \$((\${PIPESTATUS[0]} | \${PIPESTATUS[1]}));";
#print "command >$psqlCommand<\n";

  printf("(pid:$$) %s: start:transferring $table\n", timenow() );
  my $returnCode = int( system($psqlCommand)>>8);
  printf("(pid:$$) %s:   end:transferring $table\n", timenow() );

  if (0 != $returnCode) {
    print "Error copying table $table [rc=$returnCode].  Exiting...\n";
    exit(-1); 
  } else {
    print "copied table $table successfully.\n";
  }
}


# ----------------------------------------------------------
# gets the minimum date from a range in YYYY-MM-DD format
# $1 = database handle
# $2 = table name
sub min_date {

  my ($_dbh, $_tbl) = @_;
  my $retval;

# printf("(pid:$$) %s: start:min_date ($_tbl).\n", timenow());

  my $sql = qq{SELECT to_char(min(created_at), 'YYYY-MM-DD') FROM $_tbl WHERE created_at < '$datecutoff'};
  my $sth = $_dbh->prepare($sql) or die "Couldn't prepare statement: $DBI::errstr; stopped";
  $sth->execute()                or die "Couldn't execute statement: $DBI::errstr; stopped";

  # Fetch each row and print it
  while ( my @row = $sth->fetchrow_array() ) {
    $retval = $row[0];
  }
  $retval = "NODATA" if not $retval; 
  $sth->finish();

# printf("(pid:$$) %s:   end:min_date ($_tbl).\n", timenow());
  return $retval;
}


# ----------------------------------------------------------
# $1 = db_handle
# $2 = table name
sub count_rows {

  my ($_dbh, $_tbl) = @_;

  my $sql = qq{SELECT count(*) FROM $_tbl WHERE created_at < '$datecutoff'};
#     $sql = $sql . " AND retailer_id=$retailerId";    # SWITCHED OFF pending review
  my $sth = $_dbh->prepare($sql) or die "Couldn't prepare statement: $DBI::errstr; stopped";
  $sth->execute() or die "Couldn't execute statement: $DBI::errstr; stopped";

  return $sth->fetchrow_array();
}


# ----------------------------------------------------------
# sets globals pp_host, pp_user and pp_port utilising a DBI connection
# gets details from ~/.pg_service.conf
sub retrieve_host_user_port {

  my ($_dbname_service) = @_;

  my $dbh = DBI ->connect("dbi:Pg:service=$_dbname_service",  "", "", {AutoCommit => 1, RaiseError => 1})
    or die $DBI::errstr;

  $pp_host = $dbh->{pg_host};
  $pp_user = $dbh->{pg_user};
  $pp_port = $dbh->{pg_port};

#printf "pp_host is >$pp_host<\n";
#printf "pp_user is >$pp_user<\n";
#printf "pp_port is >$pp_port<\n";

  $dbh->disconnect;
}

# ----------------------------------------------------------
# $1 = dbname service (see .pg_service.conf)
# $2 = list of tables to return counts of
sub count_projected_rows {

  my ($_dbname_service, @_tablesAll) = @_;
  my $rowcount;

  printf("(pid:$$) %s: start:count_projected_rows (service=$_dbname_service).\n", timenow());

  # gets details from ~/.pg_service.conf
  my $dbh = DBI ->connect("dbi:Pg:service=$_dbname_service",  "", "", {AutoCommit => 1, RaiseError => 1})
    or die $DBI::errstr;

  foreach my $tbl (@_tablesAll){
    $rowcount = count_rows($dbh, $tbl);
    print "  Projected count on $tbl is: $rowcount\n";
    if ("z_mcore_pos_transactions" eq $tbl and 0==$rowcount) {
      printf "** No Rows.  Correct retailer? **\n";
    }
  }
  $dbh->disconnect;
}


# ----------------------------------------------------------
# this version no longer uses the ids (as un-necessary)
# $1 = dbname service (see .pg_service.conf)
# $2 = filter flag (1=true, 0=false)
# $3 = delete flag (1=true, 0=false)
# $3 = list of table to archive
sub archive_table_data {

  my ($_dbname_service, $_filterFlag, @_table_list) = @_;

  printf ("(pid:$$) %s: start: archive_table_data (service=$_dbname_service).\n", timenow());

  my $dbh;
  eval { $dbh = DBI ->connect("dbi:Pg:service=$_dbname_service",  "", "", {AutoCommit => 1, RaiseError => 1}); };
  if ($@) { print "$@\n"; release_lock(); exit(-1); }

  foreach my $tbl (@_table_list){
    printf ("\n%s: Starting work on $tbl\n", timenow() );

    my ($filter,$this_file_name,@ids2backup,$allmatchids);
    $this_file_name = sprintf("%s_%s_%s.csv.gz", $tbl, min_date($dbh, $tbl), $datecutoff);

    if ($_filterFlag) {
      $filter = " WHERE created_at < '$datecutoff'"
    } else { $filter = ""; }

    # Copy identified rows to file
    my $dir_file_name = $localdir . $this_file_name;
  
    # Output file for copied record
    my $zippy = IO::Compress::Gzip->new( $dir_file_name) or die "Could not write to $dir_file_name: $GzipError\n";
 
    printf ("%s: Creating $dir_file_name\n", timenow() );
    eval {
      $dbh->do(qq{COPY (SELECT * FROM $tbl$filter) TO STDOUT});
      my $row = 1;
      my $counter = 0;
      do {
        my $data;
        $row = $dbh->pg_getcopydata($data);
        # above return -1 t indicate end of rows
        unless ($row == -1){
           print { $zippy } "$data";
           $counter++;
        }
      } while $row  > 1;
      $table_count{$tbl} = $counter;
    };

    if ($@){ print " Copy transaction failed !!: $@\n"; }

    push @file2upload, $dir_file_name;
  }
  # note: gzip files are closed by perl automatically but may do it explicitly (todo)
  $dbh->disconnect;

  printf ("(pid:$$) %s:   end: archive_table_data()\n", timenow() );
}


# ----------------------------------------------------------
# deletes selected data from given db.  Used to delete from production ops db
# $1 = dbname service (see .pg_service.conf)
# $2 = list of tables to delete (selected data)
sub delete_table_data {

  my ($_dbname_service, @_table_list) = @_;
  print  "---------------------------------------------------------------\n";
  printf ("(pid:$$) %s: start: delete_table_data (service=$_dbname_service).\n", timenow());

  if ($delete) {
    my $dbh = DBI ->connect("dbi:Pg:service=$_dbname_service",  "", "", {AutoCommit => 1, RaiseError => 1})
      or die $DBI::errstr;

    foreach my $tbl (@_table_list){
      my $rows_deleted;

      $rows_deleted = count_rows($dbh, $tbl);
      if ($rows_deleted != $table_count{$tbl}) { 
        print "*** ERROR\n";
        print "*** ERROR row_counts do not match !! (archived=$table_count{$tbl} deleted=$rows_deleted)\n";
        print "*** ERROR Skipping...\n";
        next;
      }
      printf ("\n%s: Deleting from $tbl\n", timenow() );

      eval {
        $rows_deleted = $dbh->do(qq{DELETE FROM $tbl WHERE created_at < '$datecutoff'});
      };
      if ($@){ print "\nDelete transaction failed. Please investigate: $@\n"; }

      print "\$rows_deleted is $rows_deleted.\n";
    }
    $dbh->disconnect;
  } else {
    print "Delete option DISABLED.\n";
  }
  printf ("(pid:$$) %s:   end: delete_table_data()\n", timenow() );
}


# ----------------------------------------------------------
# truncates all data from given db.  Used to remove transitional
# data in Pseudonymisation database which has been archived to S3.
# *** WARNING: this could be dangerous if production ops database reference is used ***
# ***          a check has been added below to lower this risk                      ***
# $1 = dbname service (see .pg_service.conf)
# $2 = list of tables to delete (selected data)
sub truncate_table_data {

  my ($_dbname_service, @_table_list) = @_;
  print   "---------------------------------------------------------------\n";
  printf ("(pid:$$) %s: start: truncate_table_data (service=$_dbname_service).\n", timenow());

  # db name truncate check
  if (not ($_dbname_service =~ m/_pp_/)) {
      print "$_dbname_service DOES NOT match the pattern (should not be truncated).  Please investigate!\n";
      return -1;
  }

  if ($truncate) {
    my $dbh = DBI ->connect("dbi:Pg:service=$_dbname_service",  "", "", {AutoCommit => 1, RaiseError => 1})
      or die $DBI::errstr;

    foreach my $tbl (@_table_list){
      printf ("\n%s: Truncating $tbl\n", timenow() );

      eval {
        my $rows_truncated = $dbh->do(qq{TRUNCATE TABLE $tbl});
print "\$rows_truncated is $rows_truncated.\n";
      };
      if ($@){
        print "\nTruncate failed. Please investigate: $@\n";
      }
    }
    $dbh->disconnect;
  } else {
    print "Truncate option DISABLED.\n";
  }
  printf ("(pid:$$) %s:   end: truncate_table_data()\n", timenow() );
}


# ----------------------------------------------------------
sub usage_info {
   die  "You must provide the required parameters:\n$0 [parameters]
   -r|--retailer <retailer_name>
   -d|--database <database>

    Optional parameters are:
   --delete also deletes backed up rows
   -b|--backup remote directory within s3 bucket to save backup
   -s|--s3-bucket bucket name
   -l|--localdir local directory to store file prior to upoad
   -t|--tables tables to backup
   -c|--countonly show project count of rows (and exit)
   -?|--help
   \n";
}

