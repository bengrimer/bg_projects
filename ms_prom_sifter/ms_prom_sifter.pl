#!/usr/bin/perl
# this script is used to sift promotion details from the log files of M&S. The original
# version was a bash script (developed for Pingo Doce) which did not have the same format.

# It is intended to be used in the following way:

# cd <M&S logfiles location>
# for f in $(ls -1rt server.log.YYYY-MM*); do (nice) grep -A100 "id=<prom. id>" $f | ~/ms_prom_sifter.pl; done

# this will output similar to below:
#
# 2018-03-05 18:10:04: (promo) 4908 (max) 0
# 2018-03-05 18:16:32: (promo) 4908 (max) 0

use strict;
use warnings;

my $date1;
#my $limit1;
my $slpp;
my $slpc;
my $promo1;
my $block=0;
my $inDateTrig=0;
my $startDate;
my $endDate;
my ($retailer_id, $name, $status, $rank, $template_id, $promotion_type, $inputChannelId, $expiry);
my ($updatedAt, $targets, $UCpV, $servedCounter, $selectedStores);


printf("Start...\n\n");

#my $ben=" 1,2 ,3, branch 676";
#my @benary = split / *, */, $ben;
#my @benary2;

#my $oldstring = "bengregmarcgregivan";
my $oldstring = "110, 111 , 112, store 113,store 456";

#(my $newstring = $oldstring) =~ s/, (.*?) ,/#($2)#/g;
#(my $newstring = $oldstring) =~ s/([^,]*),*/"$1"/g;
#(my $newstring = $oldstring) =~ s/([^,]+)/"$1"/g;

(my $newstring = $oldstring) =~ s/( *)([^,]+)( *)/"$2"/g;


#(my $newstring = $oldstring) =~ s/([^,\s][^\,]*[^,\s]*)/"$1"/g;
# [^,]+



print "$oldstring\n";
print "$newstring\n";

#foreach my $_ben (@benary) {
#  print ">>>$_ben<<<\n";
#  push @benary2, '"'.$_ben.'"';
#}
#print "=$ben=\n\n";
#print "=" . join(',', @benary2) . "=\n\n";

exit(98);

# for each line of the input stream
while ( <>) 
{
# printf("line $_\n");

  if ($block == 0) {
    # match YYYY-MM-DD HH:MM line prefixes containing 'Promotion[id='
    if  (/^([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}).*?Promotion\[id=(.*?)\]/) {
      $promo1 = $2;
      $date1  = $1;
      $block  = 1;
    }
  } elsif ($block == 1) {  

    if (/^  retailer_id=(.*)/) {
      $retailer_id = $1;
    }
    elsif (/^  name=(.*)/) {
      $name = $1;
    }
    elsif (/^  status=(.*)/) {
      $status = $1;
    }
    elsif (/^  rank=(.*)/) {
      $rank = $1;
    }
    elsif (/^  template_id=(.*)/) {
      $template_id = $1;
    }
    elsif (/^  promotion_type=(.*)/) {
      $promotion_type = $1;
    }
    elsif (/^  inputChannelId=(.*)/) {
      $inputChannelId = $1;
    }
    elsif (/^  expiry=(.*)/) {
      $expiry = $1;
    }
    elsif (/^  updatedAt=(.*)/) {
      $updatedAt = $1;
    }
    elsif (/^  targets=(.*)/) {
      $targets = $1;
    }
    elsif (/^  unique_contents_per_voucher=(.*)/) {
      $UCpV = $1;
    }
    elsif (/^  servedCounter=(.*)/) {
      $servedCounter = $1;
    }
    elsif (/^  serve_limit_per_promotion=(.*)/) {
      $slpp = $1;
    }
    elsif (/^  serve_limit_per_customer=(.*)/) {
      $slpc = $1;
    }

    elsif (/^  selectedStores=\[(.*)\]/) {
      $selectedStores = $1;
    }

    # extract the first date
    elsif (/TriggerNode\[.*?date_start/) {
      $inDateTrig++;
    }

    # capture start date trigger
    elsif (1==$inDateTrig && /triggerValues=\[(.*?)\]/) {
      $startDate=$1;
    }

    # capture end date trigger
    elsif (2==$inDateTrig && /triggerValues=\[(.*?)\]/) {
      $endDate=$1;
      $inDateTrig++;
    }

    # once we hit another line with YYYY-MM-DD etc then output an audit message
    # the block flag allows the program to skip all the subsequent lines which are ignored

    elsif  (/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/) {
      if ($block == 1) {
        printf("%s: (promo) %s (startDateT) %s (endDateT) %s\n"
          , $date1,$promo1,$startDate,$endDate);
        printf("  (retailer_id) %s (name) >%s<\n  (status) %s (rank) %s (template_id) %s (promotion_type) %s (inputChannelId) %s (expiry) %s\n"
          , $retailer_id, $name, $status, $rank, $template_id, $promotion_type, $inputChannelId, $expiry);
        printf("  (updatedAt) %s (targets) %s (UCpV) %s (servedCounter) %s (SLPP) %s (SLPC) %s\n"
          , $updatedAt, $targets, $UCpV, $servedCounter, $slpp, $slpc);

        printf("  (selectedStores) >%s<\n", json_ss($selectedStores));
        print "-------------------------------------------------\n";
        $block = 0;
        $inDateTrig = 0;
      }
    }
  } else { print "*** This should never happen! ***\n"; }
}

# job done.  :-)
exit 0;

sub json_ss {

  my ($_ss) = @_;

  my @stores = split /,/, $_ss;

  my @stores2;
  foreach my $store (@stores) {

  }
  return ">$_ss<";
} 

