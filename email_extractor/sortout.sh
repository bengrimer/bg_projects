# script to sort output quickly

echo "sorting/uniq main list"
cat email2.out | sort | uniq > email_sortuniq.out

echo "sorting/uniq bad list"
cat email.bad | sort | uniq > email.bad_sorted

echo "ordering sortuniq main list by size"
cat email_sortuniq.out | awk '{ print length, $0 }' | sort -n -s | cut -d" " -f2- > sorted_by_length.txt

echo "ordering sortuniq bad list by size"
cat email.bad_sorted | awk '{ print length, $0 }' | sort -n -s | cut -d" " -f2- > sorted_by_length_bad.txt

