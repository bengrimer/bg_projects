This appears to be the working code for email extract. This is a project
to scan an SQL dump file and identify all the email addresses and extract
them into a sorted file.

Another script then is used to create a sorted unique list.

notes from 02/12/18 (how I left it)
-------------------

the email_extract program has now been named emex.c. The original is in git.  This one currently is not.

I was working on a function to extract tokens but I had not integrated it into the program yet.

A script, sortout.sh, is used to quickly sort and unique the output.
(into various files: by length, bad ones, and good ones)

output of emex is: email2.out (good emails) and email.bad (bad ones)

Not sure what results.out is.



Ben Grimer
20/12/18


