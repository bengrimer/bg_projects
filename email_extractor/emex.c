/*
 * Program to extract email addresses from an SQL dump file
 *
 * v0.1 - original program
 *
 *  compile: gcc email_extractor.c o email_extractor
 *
 * Ben Grimer 29/11/18
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// declarations
void init( int , char ** );
char* timenow(void);
void validate_email(char *, char *);

// global vars

char *input_file;
char *output_file;
char debug=0;
FILE *fptr_in;                                     /* input file */ 
FILE *fptr_out, *fptr_emailBad;                    /* output file */
long int _email_count=0;
long int _email_count_good=0;
long int _email_count_bad=0;
long int _warning_count=0;
char _flagWarning=0;

/****************************************************************************/
void init( int argc, char **argv )
{
  printf("argc=%i\n",argc);
  if (argc != 3 && argc != 4) {
    printf("Program accepts only 2 arguments (input and output filespec) ... Exiting\n\n");
    exit(-1);
  } else {
    if (4 == argc) { debug=1; }

    input_file = argv[1];
    output_file = argv[2];
    if (NULL == (fptr_in = fopen(input_file, "r"))) {
      printf("Could not open input file %s. Exiting ...\n", input_file);
      exit(EXIT_FAILURE);
    }
    if (NULL == (fptr_out = fopen(output_file, "w"))) {
      printf("Could not open output file %s. Exiting ...\n", output_file);
      exit(EXIT_FAILURE);
    }
    if (NULL == (fptr_emailBad = fopen("email.bad", "w"))) {
      printf("Could not open output file %s. Exiting ...\n", "email.bad");
      exit(EXIT_FAILURE);
    }
  }
}

/****************************************************************************/
int main ( int argc, char **argv )
{
   printf("\n%s: start...\n", timenow() );

  init(argc, argv);

    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    long int _blank_count=0;
    long int line_count = 0;
    char *_ptr1;
    char *_ptr0;

    while ((read = getline(&line, &len, fptr_in)) != -1) {      // printf("Retrieved line of length %zu :\n", read);
      line_count++; 

      _ptr1 = strstr(line, "@");    // strstr(haystack, needle);
      if (0 != _ptr1) {

        for (_ptr0 = _ptr1; *_ptr0!=' '; _ptr0--) {
          if ('>'==*_ptr0) break;
          if ('['==*_ptr0) break;
        }
        if (' '==*_ptr0) { _flagWarning=1; } 

        _ptr0++;
        for (; *_ptr1!=' '; _ptr1++) {
          if ('<' ==*_ptr1) break;
          if (']' ==*_ptr1) break;
          if (')' ==*_ptr1) break;
          if ('\0'==*_ptr1) {_flagWarning=1; break;};
        }
        if (' '==*_ptr1) { _flagWarning=1; } 

        *_ptr1 = 0;
        if (_ptr1-_ptr0 < 2) {
          if ('@'!=*_ptr0) { printf("%s not @\n", _ptr0); }
          _blank_count++;
        } else {
          validate_email(_ptr0, _ptr1-1);
        }
      }

      if (0==line_count%500000) { printf("%s: #%li\n", timenow(), line_count); }
//    if (line) free(line);       /* cleanup */
    }
    printf("%li line read from input file. email_count=%li. blank_count=%li. warning_count=%li.\n"
      , line_count, _email_count, _blank_count, _warning_count);

    if (line) free(line);       /* cleanup */
    fclose(fptr_in);
    fclose(fptr_out);
 
    exit(EXIT_SUCCESS);
}

// ******************************************************************
char* timenow(void)
{
  static time_t rawtime;
  static struct tm *tm;
  static char _buf[20+1];
         char* _bufp = _buf;

  time ( &rawtime );
  tm = localtime ( &rawtime );
  
  sprintf (_bufp, "%04d-%02d-%02d %02d:%02d:%02d",
    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
  return _bufp;
}

// ******************************************************************
void validate_email(char * _email, char *_lastChar)
{

  if ('@'==*_lastChar || '@'==*_email) {
    fprintf(fptr_emailBad, "%s\n", _email);
  } else if (strstr(_email, "\\n")>0 ) {
    fprintf(fptr_emailBad, "%s\n", _email);
  } else {
    switch (*_email)
    {
      case '`':
      case '[':
      case '*':
      case '\\':
        fprintf(fptr_emailBad, "%s\n", _email);
        break;
//    case '-':
//    case '_':
//    case '\\':
//      if ('@'==*_lastChar) { fprintf(fptr_emailBad, "%s\n", _email); break; }

      default:
        fprintf(fptr_out, "email>%s<\n", _email);
    }
  }

  if (_flagWarning) {
    printf("Warning email>%s<\n", _email);
    _warning_count++;
    _flagWarning=0;
  }
  _email_count++;

}

// ******************************************************************
void remove_token(char * _fullStr, char *_delim)
{


}

/*
int main () {
   char str[80] = "This is - www.tutorialspoint.com - website";
   const char s[2] = "-";
   char *token;
   
   * get the first token *
   token = strtok(str, s);
   
   * walk through other tokens *
   while( token != NULL ) {
      printf( " %s\n", token );
    
      token = strtok(NULL, s);
   }
   
   return(0);
}
*/

/* the end */

