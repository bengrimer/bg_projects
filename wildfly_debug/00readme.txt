This folder contains 2 script which are used for turning on and off debug
on a Wildfly server. This is done via a command line interface (CLI). This
was used successfully to diagnose a bug at M&S where debug was switched on
during a transaction for 5 minutes and then switched off again once the 
transaction was complete.

Ben Grimer
31/10/18

