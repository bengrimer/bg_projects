/srv/wildfly-ms/bin/jboss-cli.sh --controller=http-remoting://localhost:9990 << EOF
connect
ben.grimer
Dh4\$6A
# set DEBUG on ecrebo classes and root logger
/subsystem=logging/logger=com.ecrebo.mco:write-attribute(name=level,value=DEBUG)
/subsystem=logging/periodic-rotating-file-handler=FILE:write-attribute(name=level, value=DEBUG)
# show the ROOT logger level
/subsystem=logging/periodic-rotating-file-handler=FILE:read-attribute(name=level)
# show log level for class com.ecrebo.mco (replace with other classes)
/subsystem=logging/logger=com.ecrebo.mco:read-attribute(name=level)
EOF
printf "The End.\n\n"

