#include <stdio.h>

// declarations
void main_logic();
void show_menus();
int read_options();

// main program
void main() {

  printf("hello world.\n\n");

  main_logic();
}


// ----------------------------------
// Step #4: Main logic - infinite loop
// ------------------------------------

void main_logic() {

  while (1) {
    show_menus();
    read_options();
  }

}

// function to display menus
void show_menus() {

//  clear
  printf("~~~~~~~~~~~~~~~~~~~~~\n");
  printf(" M A I N - M E N U   \n");
  printf("~~~~~~~~~~~~~~~~~~~~~\n");
  printf("1. Set Terminal\n");
  printf("2. Reset Terminal\n");
  printf("3. Prod Menu\n");
  printf("4. Exit\n");
  printf("Enter choice [1-4]:" );

}

int read_options() {

  static int _choice;

  scanf("%d", &_choice);
  return _choice;
}

/*

#!/bin/bash
# A menu driven shell script sample template 
## ----------------------------------
# Step #1: Define variables
# ----------------------------------
EDITOR=vim
PASSWD=/etc/passwd
RED='\033[0;41;30m'
STD='\033[0;0;39m'
 
# ----------------------------------
# Step #2: User defined function
# ----------------------------------
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

one(){
	echo "one() called"
        pause
}
 
# do something in two()
two(){
	echo "two() called"
        pause
}
 

# function to display menus
prod_menu() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " P R O D - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. M&S"
	echo "2. Waitrose"
	echo "3. Main Menu"
        pause
}

# function to display menus
show_menus() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " M A I N - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. Set Terminal"
	echo "2. Reset Terminal"
	echo "3. Prod Menu"
	echo "4. Exit"
}
# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
read_options(){
	local choice
	read -p "Enter choice [ 1 - 4] " choice
	case $choice in
		1) one ;;
		2) two ;;
		3) prod_menu;;
		4) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}
 
# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP
 
# -----------------------------------
# Step #4: Main logic - infinite loop
# ------------------------------------
while true
do
 
	show_menus
	read_options
done
*/

