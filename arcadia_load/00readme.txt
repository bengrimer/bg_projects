Description of Arcadia email resender files.
Ben Grimer
04/04/18

00readme.txt - this file

a.out - executable of arc1.c

arc1.c - This program reads email details found in email.in and generates JSON POST files in 1..10 subdir. structure

backup - backup of this folder (gzipped)

data - approx 1-11 subdirs containing to_send, sent, and error folders containing original input and output files.

email.in.gz - list of email addresses with unsent DRs + original trans. uuid and selected_choices details
              These were taken from a table called email_trans (which was removed from production on 27/06/18
CREATE TABLE "public"."Untitled" (
  "transaction_uuid" uuid,
  "already_sent" bool,
  "recipient_email_address" varchar(255) COLLATE "pg_catalog"."default",
  "action" varchar(30) COLLATE "pg_catalog"."default",
  "optins" varchar(255) COLLATE "pg_catalog"."default"
)
;

ALTER TABLE "public"."Untitled" 
  OWNER TO "usr_beng";

misc - odds and sods

resend_arcadia_dr.sh - main bash script to send (approx. 100,000) DR emails.

resend_dr.log - log details for each DR.  Time, filename and status


