#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// declarations
void array_function(int _ary[]);
void array_function2(int*);

/****************************************************************************/
int main ( int argc, char **argv )
{
  int aryG[] = {1,2,3,4,5,6};

  int* alt_aryG = aryG;
  alt_aryG++;

  printf("sizeof aryG is %li\n", sizeof(aryG)/sizeof(aryG[0]));

  printf("sizeof ALTaryG is %li\n", sizeof(alt_aryG));

  array_function(aryG);
//array_function2(alt_aryG);
  exit(EXIT_SUCCESS);
}

/* the end */

// ********************************************************************
void array_function(int _ary[]) {

printf("sizeof evaluates to: %li\n", sizeof(_ary) );




  for (int i1=0; i1< sizeof(_ary)/sizeof(_ary[0]); i1++) {
//for (int i1=0; i1< 6; i1++) {
    printf("element %i is %i\n", i1, _ary[i1]);
  }
}

// ********************************************************************
void array_function2(int* _ary) {

  for (int i1=0; i1< 6; i1++) {
    printf("element %i is %i\n", i1, _ary[i1]);
  }

}
